# README #

SimpleSyndicate.EntityFrameworkCore NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.EntityFrameworkCore

### What is this repository for? ###

* Common functionality for EF Core applications, primarily having an audit trail in a context.

### Reporting issues ###
* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.entityframeworkcore/issues?status=new&status=open
