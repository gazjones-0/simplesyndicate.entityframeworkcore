﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleSyndicate.EntityFrameworkCore.Models
{
    /// <summary>
    /// Version history item.
    /// </summary>
    public class VersionHistoryItem
    {
        /// <summary>
        /// Gets or sets the id of the version history item.
        /// </summary>
        /// <value>An integer identifying the version history item.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the major version number.
        /// </summary>
        /// <value>An integer holding the major version number.</value>
        [Required]
        public int MajorVersion { get; set; }

        /// <summary>
        /// Gets or sets the minor version number.
        /// </summary>
        /// <value>An integer holding the minor version number.</value>
        [Required]
        public int MinorVersion { get; set; }

        /// <summary>
        /// Gets or sets the point version number.
        /// </summary>
        /// <value>An integer holding the point version number.</value>
        [Required]
        public int PointVersion { get; set; }

        /// <summary>
        /// Gets or sets the release date.
        /// </summary>
        /// <value>The release date.</value>
        [Required]
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets or sets the release notes.
        /// </summary>
        /// <value>The release notes.</value>
        [Required]
        public string ReleaseNotes { get; set; }
    }
}
