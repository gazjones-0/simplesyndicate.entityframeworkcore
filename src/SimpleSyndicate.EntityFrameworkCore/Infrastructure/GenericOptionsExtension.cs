﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using Microsoft.Extensions.DependencyInjection;

// use this namespace so users don't need to add an explicit reference
namespace Microsoft.EntityFrameworkCore.Infrastructure
{
    /// <summary>
    /// <para>
    /// Represents generic options. These options are set using <see cref="DbContextOptionsBuilder" />.
    /// </para>
    /// <para>
    /// Instances of this class are designed to be immutable. To change an option, call one of the 'With...'
    /// methods to obtain a new instance with the option changed.
    /// </para>
    /// </summary>
    public class GenericOptionsExtension : IDbContextOptionsExtension
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericOptionsExtension"/> class with everything set to default values.
        /// </summary>
        protected GenericOptionsExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericOptionsExtension"/> class, called by a derived class constructor when implementing the <see cref="Clone"/> method.
        /// </summary>
        /// <param name="copyFrom">The instance that is being cloned.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801", Justification = "Included by convention, and used by Clone; derived classes may need this.")]
        protected GenericOptionsExtension(GenericOptionsExtension copyFrom)
        {
        }

        /// <summary>
        /// Gets a message fragment for logging typically containing information about
        /// any useful non-default options that have been configured.
        /// </summary>
        /// <value>Log fragment.</value>
        public virtual string LogFragment => string.Empty;

        /// <summary>
        /// Adds the services required to make the selected options work. This is used when there
        /// is no external <see cref="IServiceProvider" /> and EF is maintaining its own service
        /// provider internally. This allows database providers (and other extensions) to register their
        /// required services when EF is creating an service provider.
        /// </summary>
        /// <param name="services">The collection to add services to.</param>
        /// <returns><c>true</c> if a database provider and was registered; <c>false</c> otherwise.</returns>
        public virtual bool ApplyServices(IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            return true;
        }

        /// <summary>
        /// Returns a hash code created from any options that would cause a new <see cref="IServiceProvider" />
        /// to be needed. Most extensions do not have any such options and should return zero.
        /// </summary>
        /// <returns>A hash over options that require a new service provider when changed.</returns>
        public virtual long GetServiceProviderHashCode() => 0L;

        /// <summary>
        /// Gives the extension a chance to validate that all options in the extension are valid.
        /// Most extensions do not have invalid combinations and so this will be a no-op.
        /// If options are invalid, then an exception should be thrown.
        /// </summary>
        /// <param name="options">The options being validated.</param>
        public virtual void Validate(IDbContextOptions options)
        {
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>A clone of this instance, which can be modified before being returned as immutable.</returns>
        protected virtual GenericOptionsExtension Clone() => new GenericOptionsExtension(this);
    }
}
