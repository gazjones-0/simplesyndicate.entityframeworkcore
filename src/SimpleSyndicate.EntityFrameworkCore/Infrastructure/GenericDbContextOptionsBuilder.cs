﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.ComponentModel;

// use this namespace so users don't need to add an explicit reference
namespace Microsoft.EntityFrameworkCore.Infrastructure
{
    /// <summary>
    /// <para>
    /// Allows generic configuration to be performed on <see cref="DbContextOptions" />.
    /// </para>
    /// <para>
    /// Instances of this class are typically returned from methods that configure the context to use a
    /// particular relational database provider.
    /// </para>
    /// </summary>
    /// <typeparam name="TBuilder">Type of builder.</typeparam>
    /// <typeparam name="TExtension">Type of extension.</typeparam>
    public abstract class GenericDbContextOptionsBuilder<TBuilder, TExtension> : IGenericDbContextOptionsBuilderInfrastructure
        where TBuilder : GenericDbContextOptionsBuilder<TBuilder, TExtension>
        where TExtension : GenericOptionsExtension, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDbContextOptionsBuilder{TBuilder, TExtension}"/> class using the specified <paramref name="optionsBuilder"/>
        /// </summary>
        /// <param name="optionsBuilder">Options builder to use.</param>
        protected GenericDbContextOptionsBuilder(DbContextOptionsBuilder optionsBuilder) => OptionsBuilder = optionsBuilder ?? throw new ArgumentNullException(nameof(optionsBuilder));

        /// <summary>
        /// Gets the core options builder.
        /// </summary>
        /// <value>Options builder.</value>
        DbContextOptionsBuilder IGenericDbContextOptionsBuilderInfrastructure.OptionsBuilder => OptionsBuilder;

        /// <summary>
        /// Gets the core options builder.
        /// </summary>
        /// <value>Options builder.</value>
        protected virtual DbContextOptionsBuilder OptionsBuilder { get; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string ToString() => base.ToString();

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the specified object is equal to the current object; otherwise, <c>false</c>.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj) => base.Equals(obj);

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode() => base.GetHashCode();

        /// <summary>
        /// Sets an option by cloning the extension used to store the settings. This ensures the builder
        /// does not modify options that are already in use elsewhere.
        /// </summary>
        /// <param name="setAction">An action to set the option.</param>
        /// <returns>The same builder instance so that multiple calls can be chained.</returns>
        protected virtual TBuilder WithOption(Func<TExtension, TExtension> setAction)
        {
            ((IDbContextOptionsBuilderInfrastructure)OptionsBuilder).AddOrUpdateExtension(setAction(OptionsBuilder.Options.FindExtension<TExtension>() ?? new TExtension()));
            return (TBuilder)this;
        }
   }
}
