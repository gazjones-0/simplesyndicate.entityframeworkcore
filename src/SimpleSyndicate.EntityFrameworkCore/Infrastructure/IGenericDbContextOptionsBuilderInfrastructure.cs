﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

// use this namespace so users don't need to add an explicit reference
namespace Microsoft.EntityFrameworkCore.Infrastructure
{
    /// <summary>
    /// Explicitly implemented by <see cref="GenericDbContextOptionsBuilder{TBuilder, TExtension}" /> to hide
    /// methods that are used by database provider extension methods but not intended to be called by application
    /// developers.
    /// </summary>
    public interface IGenericDbContextOptionsBuilderInfrastructure
    {
        /// <summary>
        /// Gets the core options builder.
        /// </summary>
        /// <value>Options builder.</value>
        DbContextOptionsBuilder OptionsBuilder { get; }
    }
}
