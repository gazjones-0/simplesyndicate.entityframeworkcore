﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models
{
    /// <summary>
    /// Change sets record changes (creates, updates and deleted) made to entities in a data store; a single change set
    /// can contain changes to multiple entities.
    /// <note type="important">
    /// Entities are modeled as objects with properties.
    /// </note>
    /// </summary>
    /// <see cref="AuditObjectChange"/>
    /// <see cref="AuditPropertyChange"/>
    /// <see cref="Auditing"/>
    [Table("AuditChangeSets")]
    public class AuditChangeSet
    {
        /// <summary>
        /// Gets or sets the id of the change set.
        /// </summary>
        /// <value>An integer that identifies the change set.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp of when the changes were made.
        /// </summary>
        /// <value>A timestamp of when the changes were made.</value>
        [Required]
        [DataType(DataType.Date)]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the user that made the changes.
        /// </summary>
        /// <value>A string that identifies the user that made the changes.</value>
        [MaxLength(DataAnnotations.AuditChangeSetDataAnnotations.UserId.MaxLength)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the collection of all the <see cref="AuditObjectChange"/>s in the change set.
        /// </summary>
        /// <value>The object changes that make up the change set.</value>
        public ICollection<AuditObjectChange> ObjectChanges { get; set; }
    }
}
