﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models.DataAnnotations.AuditObjectChangeDataAnnotations
{
    /// <summary>
    /// Data annotations for <see cref="AuditObjectChange.TypeName"/>
    /// </summary>
    public static class TypeName
    {
        /// <summary>
        /// Maximum length of <see cref="AuditObjectChange.TypeName"/>.
        /// </summary>
        public const int MaxLength = 128;
    }
}
