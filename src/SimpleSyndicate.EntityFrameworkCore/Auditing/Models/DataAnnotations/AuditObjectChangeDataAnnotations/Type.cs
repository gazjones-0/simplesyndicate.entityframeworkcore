﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models.DataAnnotations.AuditObjectChangeDataAnnotations
{
    /// <summary>
    /// Data annotations for <see cref="AuditObjectChange.ChangeType"/>
    /// </summary>
    public static class Type
    {
        /// <summary>
        /// Minimum length of <see cref="AuditObjectChange.ChangeType"/>.
        /// </summary>
        public const int MinLength = 1;

        /// <summary>
        /// Maximum length of <see cref="AuditObjectChange.ChangeType"/>.
        /// </summary>
        public const int MaxLength = 1;
    }
}
