﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models.DataAnnotations.AuditObjectChangeDataAnnotations
{
    /// <summary>
    /// Data annotations for <see cref="AuditObjectChange.KeyValue"/>
    /// </summary>
    public static class KeyValue
    {
        /// <summary>
        /// Maximum length of <see cref="AuditObjectChange.KeyValue"/>.
        /// </summary>
        public const int MaxLength = 128;
    }
}
