﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models.DataAnnotations.AuditPropertyChangeDataAnnotations
{
    /// <summary>
    /// Data annotations for <see cref="AuditPropertyChange.PropertyName"/>
    /// </summary>
    public static class PropertyName
    {
        /// <summary>
        /// Minimum length of <see cref="AuditPropertyChange.PropertyName"/>.
        /// </summary>
        public const int MaxLength = 128;
    }
}
