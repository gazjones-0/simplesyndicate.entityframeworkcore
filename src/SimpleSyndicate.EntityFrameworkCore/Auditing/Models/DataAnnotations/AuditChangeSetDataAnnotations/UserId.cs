﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models.DataAnnotations.AuditChangeSetDataAnnotations
{
    /// <summary>
    /// Data annotations for <see cref="AuditChangeSet.UserId"/>
    /// </summary>
    public static class UserId
    {
        /// <summary>
        /// Maximum length of <see cref="AuditChangeSet.UserId"/>.
        /// </summary>
        public const int MaxLength = 128;
    }
}
