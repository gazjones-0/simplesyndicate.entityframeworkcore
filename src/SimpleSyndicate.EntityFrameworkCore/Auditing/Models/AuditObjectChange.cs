﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models
{
    /// <summary>
    /// A change made to an entity in a data store.
    /// </summary>
    /// <see cref="AuditChangeSet"/>
    /// <see cref="AuditPropertyChange"/>
    /// <see cref="Auditing"/>
    [Table("AuditObjectChanges")]
    public class AuditObjectChange
    {
        /// <summary>
        /// Gets or sets the id of the object change.
        /// </summary>
        /// <value>An integer that identifies the object change.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type name of the object that has been changed.
        /// </summary>
        /// <value>A string containing the type name of the object that has been changed.</value>
        [Required]
        [MaxLength(DataAnnotations.AuditObjectChangeDataAnnotations.TypeName.MaxLength)]
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the key value identifying the entity that has been changed.
        /// </summary>
        /// <value>The value of the key that identifies the entity that has been changed.</value>
        [Required]
        [MaxLength(DataAnnotations.AuditObjectChangeDataAnnotations.KeyValue.MaxLength)]
        public string KeyValue { get; set; }

        /// <summary>
        /// Gets or sets the key value identifying the entity that has been changed, but as an integer.
        /// <note type="note">
        /// This holds the same value as <see cref="KeyValue"/>, but is present to make querying easier; if the key value cannot be
        /// converted to an integer this will be <c>null</c>.
        /// </note>
        /// </summary>
        /// <value>The value of the key that identifies the object that has been changed, as an integer.</value>
        public int? KeyValueAsInt { get; set; }

        /// <summary>
        /// Gets or sets the type of change -- 'C' is an entity has been created, 'U' is an entity has been updated and 'D' indicates an entity has been deleted.
        /// </summary>
        /// <value>The type of change -- 'C' if an entity has been created, 'U' if an entity has been updated or 'D' if an entity has been deleted.</value>
        [Required]
        [MinLength(DataAnnotations.AuditObjectChangeDataAnnotations.Type.MinLength)]
        [MaxLength(DataAnnotations.AuditObjectChangeDataAnnotations.Type.MaxLength)]
        public string ChangeType { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="AuditChangeSet"/> this change is part of.
        /// </summary>
        /// <value>The change set this change is part of.</value>
        [Required]
        public AuditChangeSet ChangeSet { get; set; }

        /// <summary>
        /// Gets or sets the collection of all the <see cref="AuditPropertyChange"/>s that were part of this change.
        /// </summary>
        /// <value>The property changes that make up the object change.</value>
        public ICollection<AuditPropertyChange> PropertyChanges { get; set; }
    }
}
