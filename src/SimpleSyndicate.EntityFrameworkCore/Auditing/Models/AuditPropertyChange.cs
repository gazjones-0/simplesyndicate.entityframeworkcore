﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Models
{
    /// <summary>
    /// Details of a property change.
    /// </summary>
    /// <see cref="AuditChangeSet"/>
    /// <see cref="AuditObjectChange"/>
    /// <see cref="Auditing"/>
    [Table("AuditPropertyChanges")]
    public class AuditPropertyChange
    {
        /// <summary>
        /// Gets or sets the id of the property change.
        /// </summary>
        /// <value>An integer identifying the property change.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the property that has been changed.
        /// </summary>
        /// <value>A string containing the name of the property that has been changed.</value>
        [Required]
        [MaxLength(DataAnnotations.AuditPropertyChangeDataAnnotations.PropertyName.MaxLength)]
        public string PropertyName { get; set; }

        /// <summary>
        /// Gets or sets the value of the property that has been changed.
        /// For creation, there's one <see cref="AuditPropertyChange"/> per property, and this holds the value the property was populated with at creation;
        /// for updates, there's one <see cref="AuditPropertyChange"/> per updated property, and this holds the value the property was updated with;
        /// for deletes, there's one <see cref="AuditPropertyChange"/> per property, and this holds the value the property was when the entity was deleted.
        /// </summary>
        /// <value>A string containing the value of the property.</value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="AuditObjectChange"/> this property change is part of.
        /// </summary>
        /// <value>The object change this property change is part of.</value>
        public AuditObjectChange ObjectChange { get; set; }
    }
}
