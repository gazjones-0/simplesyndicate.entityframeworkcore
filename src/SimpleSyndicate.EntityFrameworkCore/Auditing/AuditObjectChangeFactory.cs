﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Models;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Factory class for <see cref="AuditObjectChange"/>s.
    /// </summary>
    internal static class AuditObjectChangeFactory
    {
        /// <summary>
        /// Gets an <see cref="AuditObjectChange"/> for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>An <see cref="AuditObjectChange"/>.</returns>
        public static AuditObjectChange Create(EntityEntry entityEntry)
        {
            var auditObjectChange = new AuditObjectChange
            {
                TypeName = AuditObjectChangeHelpers.TypeName(entityEntry)
            };
            switch (entityEntry.State)
            {
                case EntityState.Added:
                    auditObjectChange.ChangeType = "C";
                    auditObjectChange.KeyValue = AuditObjectChangeHelpers.PrimaryKeyCurrentValue(entityEntry);
                    break;

                case EntityState.Modified:
                    auditObjectChange.ChangeType = "U";
                    auditObjectChange.KeyValue = AuditObjectChangeHelpers.PrimaryKeyOriginalValue(entityEntry);
                    break;

                case EntityState.Deleted:
                    auditObjectChange.ChangeType = "D";
                    auditObjectChange.KeyValue = AuditObjectChangeHelpers.PrimaryKeyOriginalValue(entityEntry);
                    break;
            }

            auditObjectChange.KeyValueAsInt = AuditingHelpers.ToInt(auditObjectChange.KeyValue);
            auditObjectChange.PropertyChanges = AuditPropertyChangesFactory.Create(entityEntry);
            return auditObjectChange;
        }
    }
}
