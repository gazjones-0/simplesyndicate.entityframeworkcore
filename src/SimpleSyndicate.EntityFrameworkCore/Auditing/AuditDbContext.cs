﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure.Internal;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Models;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// <para>
    /// A DbContext instance represents a session with the database and can be used to query and save
    /// instances of your entities. DbContext is a combination of the Unit Of Work and Repository patterns.
    /// </para>
    /// <para>
    /// This version provides an audit trail via the <see cref="AuditChangeSets"/>, <see cref="AuditObjectChanges"/> and <see cref="AuditPropertyChanges"/>
    /// entities; the audit trail is automatically added to whenever <c>SaveChanges</c> or <c>SaveChangesAsync</c> are called.
    /// </para>
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         Typically you create a class that derives from DbContext and contains <see cref="DbSet{TEntity}" />
    ///         properties for each entity in the model. If the <see cref="DbSet{TEntity}" /> properties have a public setter,
    ///         they are automatically initialized when the instance of the derived context is created.
    ///     </para>
    ///     <para>
    ///         Override the <see cref="OnConfiguring(DbContextOptionsBuilder)" /> method to configure the database (and
    ///         other options) to be used for the context. Alternatively, if you would rather perform configuration externally
    ///         instead of inline in your context, you can use <see cref="DbContextOptionsBuilder{TContext}" />
    ///         (or <see cref="DbContextOptionsBuilder" />) to externally create an instance of <see cref="DbContextOptions{TContext}" />
    ///         (or <see cref="DbContextOptions" />) and pass it to a base constructor of <see cref="DbContext" />.
    ///     </para>
    ///     <para>
    ///         The model is discovered by running a set of conventions over the entity classes found in the
    ///         <see cref="DbSet{TEntity}" /> properties on the derived context. To further configure the model that
    ///         is discovered by convention, you can override the <see cref="DbContext.OnModelCreating(ModelBuilder)" /> method.
    ///     </para>
    /// </remarks>
    public class AuditDbContext : DbContext
    {
        /// <summary>
        /// Holds the options for the context.
        /// </summary>
        private DbContextOptions _options;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditDbContext"/> class. The
        /// <see cref="OnConfiguring(DbContextOptionsBuilder)"/>
        /// method will be called to configure the database (and other options) to be used for this context.
        /// </summary>
        public AuditDbContext()
            : this(new DbContextOptions<AuditDbContext>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditDbContext" /> class using the specified options.
        /// The <see cref="OnConfiguring(DbContextOptionsBuilder)" /> method will still be called to allow further
        /// configuration of the options.
        /// </summary>
        /// <param name="options">The options for this context.</param>
        public AuditDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets  <see cref="DbSet{TEntity}"/> of <see cref="AuditChangeSet"/> entities.
        /// </summary>
        /// <value><see cref="DbSet{TEntity}"/> of <see cref="AuditChangeSet"/> entities.</value>
        public DbSet<AuditChangeSet> AuditChangeSets { get; set; }

        /// <summary>
        /// Gets or sets  <see cref="DbSet{TEntity}"/> of <see cref="AuditObjectChange"/> entities.
        /// </summary>
        /// <value><see cref="DbSet{TEntity}"/> of <see cref="AuditObjectChange"/> entities.</value>
        public DbSet<AuditObjectChange> AuditObjectChanges { get; set; }

        /// <summary>
        /// Gets or sets  <see cref="DbSet{TEntity}"/> of <see cref="AuditPropertyChange"/> entities.
        /// </summary>
        /// <value><see cref="DbSet{TEntity}"/> of <see cref="AuditPropertyChange"/> entities.</value>
        public DbSet<AuditPropertyChange> AuditPropertyChanges { get; set; }

        /// <summary>
        /// Saves all changes made in this context to the database.
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">Indicates whether <see cref="ChangeTracker.AcceptAllChanges" /> is called after the changes have been sent successfully to the database.</param>
        /// <remarks>
        /// <para>Unlike the standard <see cref="SaveChanges(bool)"/>, this method will always call <see cref="ChangeTracker.DetectChanges"/> to discover any changes for the audit trail.</para>
        /// <para>Unlike the standard <see cref="SaveChanges(bool)"/>, <paramref name="acceptAllChangesOnSuccess"/> must always be <c>true</c> to allow the audit trail to be added.</para>
        /// </remarks>
        /// <returns>The number of state entries written to the database.</returns>
        /// <exception cref="DoNotAcceptAllChangesNotSupportedException">Throw when <paramref name="acceptAllChangesOnSuccess"/> is not <c>true</c>.</exception>
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            if (acceptAllChangesOnSuccess == false)
            {
                throw new DoNotAcceptAllChangesNotSupportedException("Accept all changes set to false is not supported as it must be true to add the audit trail.");
            }

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                SaveChangesWithAuditTrail();
                var result = base.SaveChanges(true);
                scope.Complete();
                return result;
            }
        }

        /// <summary>
        /// Asynchronously saves all changes made in this context to the database.
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">Indicates whether <see cref="ChangeTracker.AcceptAllChanges" /> is called after the changes have been sent successfully to the database.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
        /// <remarks>
        /// <para>Unlike the standard <see cref="SaveChanges(bool)"/>, this method will always call <see cref="ChangeTracker.DetectChanges"/> to discover any changes for the audit trail.</para>
        /// <para>Unlike the standard <see cref="SaveChanges(bool)"/>, <paramref name="acceptAllChangesOnSuccess"/> must always be <c>true</c> to allow the audit trail to be added.</para>
        /// </remarks>
        /// <returns>The number of state entries written to the database.</returns>
        /// <exception cref="DoNotAcceptAllChangesNotSupportedException">Throw when <paramref name="acceptAllChangesOnSuccess"/> is not <c>true</c>.</exception>
        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (acceptAllChangesOnSuccess == false)
            {
                throw new DoNotAcceptAllChangesNotSupportedException("Accept all changes set to false is not supported as it must be true to add the audit trail.");
            }

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                SaveChangesWithAuditTrail();
                var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken).ConfigureAwait(false);
                scope.Complete();
                return result;
            }
        }

        /// <summary>
        /// Saves the changes made in the context, and adds an audit trail.
        /// </summary>
        protected void SaveChangesWithAuditTrail()
        {
            // the trail is a list of all objects that have changed, and all the properties on them that have changed
            //
            // we do this in four stages:
            //   1. Add audit trail of modified entities
            //   2. Save the changes so far
            //   3. Add audit trail of added or deleted entities
            //   4. Save any remaining changes
            //
            // we add the audit trail of modified entities before saving changes as we determine changes by comparing
            // what's in our context with what's in the database; if we did it after saving we wouldn't see any change
            //
            // we save the changes before adding the audit trail of added or deleted entities so that the values for
            // any auto-generated properties have been generated; if we did it before we wouldn't know these (e.g.
            // identity columns) which would make the audit trail very difficult to use
            //
            // it is assumed that this method will be called from within a transaction to make it atomic

            // each save is one change set
            var auditChangeSet = new AuditChangeSet
            {
                Timestamp = DateTime.UtcNow,
                UserId = AuditChangeSetHelpers.UserId(_options.FindExtension<AuditingOptionsExtension>().AuditUserAccessor)
            };

            // work out changes for modified entities; we do this before saving as we determine changes by comparing
            // what's in our context with what's in database (if we did it after we wouldn't see any changes)
            var auditObjectChanges = new List<AuditObjectChange>();
            foreach (var entry in ChangeTracker.Entries().Where(x => x.State == EntityState.Modified))
            {
                auditObjectChanges.Add(AuditObjectChangeFactory.Create(entry));
            }

            // save changes (note this is just changes by the application, the audit trail hasn't been added to the
            // context yet); however, don't accept them --  this ensures any auto-generated properties are populated
            ChangeTracker.DetectChanges();
            base.SaveChanges(false);

            // work out changes for added or deleted entities
            foreach (var entry in ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Deleted))
            {
                auditObjectChanges.Add(AuditObjectChangeFactory.Create(entry));
            }

            // accept all changes (note this is just changes by the application, audit trail hasn't been added to context yet)
            ChangeTracker.AcceptAllChanges();

            // attach audit trail to context, but only if there were changes (avoids having empty change sets cluttering up the log)
            if (auditObjectChanges.Count > 0)
            {
                auditChangeSet.ObjectChanges = auditObjectChanges;
                Set<AuditChangeSet>().Add(auditChangeSet);
            }

            // detect any remaining changes
            ChangeTracker.DetectChanges();
        }

        /// <summary>
        /// Configures.
        /// </summary>
        /// <param name="optionsBuilder">A builder used to create or modify options for this context. Databases (and other extensions) typically define extension methods on this object that allow you to configure the context.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (optionsBuilder.Options.FindExtension<AuditingOptionsExtension>() == null)
            {
                optionsBuilder.UseAuditing(o => o.UseAuditUserAccesor(new NoAuditUserAuditUserAccessor()));
            }

            _options = optionsBuilder.Options;
        }
    }
}
