﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Helper methods for auditing.
    /// </summary>
    public static class AuditingHelpers
    {
        /// <summary>
        /// Trims a string to the specified length.
        /// </summary>
        /// <param name="value">String to trim.</param>
        /// <param name="length">Maximum length of the string.</param>
        /// <returns>The trimmed string; if the input was <c>null</c>, the returned value is also <c>null</c>.</returns>
        public static string TrimToLength(string value, int length) => value != null ? (value.Length > length ? value.Substring(0, length) : value) : null;

        /// <summary>
        /// Converts the specified <paramref name="value"/> to an <see cref="int"/>, or <c>null</c> if it can't be converted.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value, or <c>null</c> if it couldn't be converted.</returns>
        public static int? ToInt(string value)
        {
            try
            {
                return Convert.ToInt32(value, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                return null;
            }
            catch (OverflowException)
            {
                return null;
            }
        }
    }
}
