﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Exception thrown when an attempt is made to audit an object that doesn't have a key defined.
    /// </summary>
    [Serializable]
    public class DoNotAcceptAllChangesNotSupportedException : NotSupportedException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotAcceptAllChangesNotSupportedException"/> class.
        /// </summary>
        public DoNotAcceptAllChangesNotSupportedException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotAcceptAllChangesNotSupportedException"/> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public DoNotAcceptAllChangesNotSupportedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotAcceptAllChangesNotSupportedException"/> class with a specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a <c>null</c> reference (<c>Nothing</c> in Visual Basic) if no inner exception is specified. </param>
        public DoNotAcceptAllChangesNotSupportedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DoNotAcceptAllChangesNotSupportedException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The <see cref="System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
        protected DoNotAcceptAllChangesNotSupportedException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
