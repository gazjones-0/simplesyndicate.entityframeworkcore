﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Interface for obtaining details of the user that is saving the changes; used by <see cref="AuditDbContext"/>.
    /// </summary>
    /// <seealso cref="AuditDbContext"/>
    public interface IAuditUserAccessor
    {
        /// <summary>
        /// Gets the id of the user that is saving changes.
        /// </summary>
        /// <value>User id.</value>
        string Id { get; }
    }
}
