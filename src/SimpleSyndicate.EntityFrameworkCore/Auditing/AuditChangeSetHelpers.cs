﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Helper methods for <see cref="Models.AuditChangeSet"/>.
    /// </summary>
    public static class AuditChangeSetHelpers
    {
        /// <summary>
        /// Gets the id of the user saving changes; if the id is longer than the audit data store can hold, it will be trimmed.
        /// </summary>
        /// <param name="auditUser">An <see cref="IAuditUserAccessor"/> that provides the user details.</param>
        /// <returns>The id of the user saving changes.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="auditUser"/> is <c>null</c>.</exception>
        public static string UserId(IAuditUserAccessor auditUser)
        {
            if (auditUser == null)
            {
                throw new ArgumentNullException(nameof(auditUser));
            }

            return AuditingHelpers.TrimToLength(auditUser.Id, Models.DataAnnotations.AuditChangeSetDataAnnotations.UserId.MaxLength);
        }
    }
}
