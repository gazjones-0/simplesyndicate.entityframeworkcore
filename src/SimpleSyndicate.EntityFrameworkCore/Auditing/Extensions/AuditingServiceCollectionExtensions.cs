﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

// use this namespace so users don't need to add an explicit reference
namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Auditing-specific extension methods for <see cref="IServiceCollection"/>.
    /// </summary>
    public static class AuditingServiceCollectionExtensions
    {
        /// <summary>
        /// <para>
        /// Adds the services required for auditing to an <see cref="IServiceCollection"/>. You use this method when using dependency injection
        /// in your application, such as with ASP.NET. For more information on setting up dependency
        /// injection, see http://go.microsoft.com/fwlink/?LinkId=526890.
        /// </para>
        /// <para>
        /// You only need to use this functionality when you want Entity Framework to resolve the services it uses
        /// from an external dependency injection container. If you are not using an external
        /// dependency injection container, Entity Framework will take care of creating the services it requires.
        /// </para>
        /// </summary>
        /// <param name="serviceCollection">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddAuditing(this IServiceCollection serviceCollection)
        {
            if (serviceCollection == null)
            {
                throw new ArgumentNullException(nameof(serviceCollection));
            }

            return serviceCollection;
        }
    }
}
