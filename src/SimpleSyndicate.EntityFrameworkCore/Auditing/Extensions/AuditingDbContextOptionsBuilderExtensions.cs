﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using Microsoft.EntityFrameworkCore.Infrastructure;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure.Internal;

// use this namespace so users don't need to add an explicit reference
namespace Microsoft.EntityFrameworkCore
{
    /// <summary>
    /// Auditing specific extension methods for <see cref="DbContextOptionsBuilder"/>.
    /// </summary>
    public static class AuditingDbContextOptionsBuilderExtensions
    {
        /// <summary>
        /// Configures the context to use auditing.
        /// </summary>
        /// <param name="optionsBuilder">The builder being used to configure the context.</param>
        /// <param name="auditingOptionsAction">An optional action to allow additional auditing-specific configuration.</param>
        /// <returns>The options builder so that further configuration can be chained.</returns>
        public static DbContextOptionsBuilder UseAuditing(this DbContextOptionsBuilder optionsBuilder, Action<AuditingDbContextOptionsBuilder> auditingOptionsAction = null)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(nameof(optionsBuilder));
            }

            var extension = optionsBuilder.Options.FindExtension<AuditingOptionsExtension>() ?? new AuditingOptionsExtension();

            ConfigureWarnings(optionsBuilder);

            ((IDbContextOptionsBuilderInfrastructure)optionsBuilder).AddOrUpdateExtension(extension);

            auditingOptionsAction?.Invoke(new AuditingDbContextOptionsBuilder(optionsBuilder));

            return optionsBuilder;
        }

        /// <summary>
        /// Configures the context to use auditing.
        /// </summary>
        /// <typeparam name="TContext">The type of context being configured.</typeparam>
        /// <param name="optionsBuilder">The builder being used to configure the context.</param>
        /// <param name="auditingOptionsAction">An optional action to allow additional auditing-specific configuration.</param>
        /// <returns>The options builder so that further configuration can be chained.</returns>
        public static DbContextOptionsBuilder<TContext> UseAuditing<TContext>(this DbContextOptionsBuilder<TContext> optionsBuilder, Action<AuditingDbContextOptionsBuilder> auditingOptionsAction = null)
            where TContext : DbContext => (DbContextOptionsBuilder<TContext>)UseAuditing((DbContextOptionsBuilder)optionsBuilder, auditingOptionsAction);

        /// <summary>
        /// Configures warnings for the <paramref name="optionsBuilder"/>.
        /// </summary>
        /// <param name="optionsBuilder">Options builder.</param>
        private static void ConfigureWarnings(DbContextOptionsBuilder optionsBuilder)
        {
            var coreOptionsExtension = optionsBuilder.Options.FindExtension<CoreOptionsExtension>() ?? new CoreOptionsExtension();

            ((IDbContextOptionsBuilderInfrastructure)optionsBuilder).AddOrUpdateExtension(coreOptionsExtension);
        }
    }
}
