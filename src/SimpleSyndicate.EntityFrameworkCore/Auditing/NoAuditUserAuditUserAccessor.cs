﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Always returns a specific user id.
    /// </summary>
    public class NoAuditUserAuditUserAccessor : IAuditUserAccessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoAuditUserAuditUserAccessor"/> class; an empty string will always be returned as the id.
        /// </summary>
        public NoAuditUserAuditUserAccessor()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoAuditUserAuditUserAccessor"/> class with the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="id">User id to always return.</param>
        public NoAuditUserAuditUserAccessor(string id) => Id = id;

        /// <inheritdoc />
        public string Id { get; }
    }
}
