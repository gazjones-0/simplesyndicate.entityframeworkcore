﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Models;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Factory class for <see cref="AuditPropertyChange"/>s.
    /// </summary>
    internal static class AuditPropertyChangesFactory
    {
        /// <summary>
        /// Gets a collection of the changes for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>A collection of the changes.</returns>
        public static List<AuditPropertyChange> Create(EntityEntry entityEntry)
        {
            var list = new List<AuditPropertyChange>();
            switch (entityEntry.State)
            {
                case EntityState.Added:
                    foreach (var property in entityEntry.CurrentValues.Properties)
                    {
                        var auditPropertyChange = new AuditPropertyChange
                        {
                            PropertyName = AuditingHelpers.TrimToLength(property.Name, Models.DataAnnotations.AuditPropertyChangeDataAnnotations.PropertyName.MaxLength),
                            Value = entityEntry.CurrentValues[property]?.ToString()
                        };
                        list.Add(auditPropertyChange);
                    }

                    break;

                case EntityState.Modified:
                    foreach (var property in entityEntry.OriginalValues.Properties)
                    {
                        if (!object.Equals(entityEntry.GetDatabaseValues()[property], entityEntry.CurrentValues[property]))
                        {
                            var auditPropertyChange = new AuditPropertyChange
                            {
                                PropertyName = AuditingHelpers.TrimToLength(property.Name, Models.DataAnnotations.AuditPropertyChangeDataAnnotations.PropertyName.MaxLength),
                                Value = entityEntry.CurrentValues[property]?.ToString()
                            };
                            list.Add(auditPropertyChange);
                        }
                    }

                    break;

                case EntityState.Deleted:
                    foreach (var property in entityEntry.OriginalValues.Properties)
                    {
                        var auditPropertyChange = new AuditPropertyChange
                        {
                            PropertyName = AuditingHelpers.TrimToLength(property.Name, Models.DataAnnotations.AuditPropertyChangeDataAnnotations.PropertyName.MaxLength),
                            Value = entityEntry.OriginalValues[property]?.ToString()
                        };
                        list.Add(auditPropertyChange);
                    }

                    break;
            }

            return list;
        }
    }
}
