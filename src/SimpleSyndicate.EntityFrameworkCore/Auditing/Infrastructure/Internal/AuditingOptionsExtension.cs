﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure.Internal
{
    /// <summary>
    /// Auditing options extension.
    /// </summary>
    public class AuditingOptionsExtension : GenericOptionsExtension, IDbContextOptionsExtension
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuditingOptionsExtension"/> class.
        /// </summary>
        public AuditingOptionsExtension()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditingOptionsExtension"/> class, copying its values from the specified <paramref name="copyFrom"/>.
        /// </summary>
        /// <param name="copyFrom">Options to copy.</param>
        public AuditingOptionsExtension(AuditingOptionsExtension copyFrom)
            : base(copyFrom) => AuditUserAccessor = copyFrom.AuditUserAccessor;

        /// <summary>
        /// Gets the log fragment.
        /// </summary>
        /// <value>Log fragment.</value>
        public override string LogFragment => "Auditing=Enabled ";

        /// <summary>
        /// Gets or sets the audit user accessor.
        /// </summary>
        /// <value>Audit user accessor.</value>
        public virtual IAuditUserAccessor AuditUserAccessor { get; set; }

        /// <summary>
        /// Returns a copy of the options with auditing added.
        /// </summary>
        /// <param name="auditUserAccessor">Audit user accessor.</param>
        /// <returns>The options.</returns>
        public virtual AuditingOptionsExtension WithAuditUserAccessor(IAuditUserAccessor auditUserAccessor)
        {
            var clone = (AuditingOptionsExtension)Clone();
            clone.AuditUserAccessor = auditUserAccessor ?? throw new ArgumentNullException(nameof(auditUserAccessor));
            return clone;
        }

        /// <summary>
        /// Adds the services required to make the selected options work. This is used when there
        /// is no external <see cref="IServiceProvider" /> and EF is maintaining its own service
        /// provider internally. This allows database providers (and other extensions) to register their
        /// required services when EF is creating an service provider.
        /// </summary>
        /// <param name="services">The collection to add services to.</param>
        /// <returns>True if a database provider and was registered; false otherwise.</returns>
        public override bool ApplyServices(IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (base.ApplyServices(services))
            {
                services.AddAuditing();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a hash code created from any options that would cause a new <see cref="IServiceProvider" /> to be needed.
        /// </summary>
        /// <returns>Hash code.</returns>
        public override long GetServiceProviderHashCode() => AuditUserAccessor?.GetHashCode() ?? 0L;

        /// <summary>
        /// Populates the debug<paramref name="debugInfo"/>.
        /// </summary>
        /// <param name="debugInfo">Debug info.</param>
        public virtual void PopulatDebugInfo(IDictionary<string, string> debugInfo) => debugInfo["Auditing"] = "Enabled";

        /// <summary>
        /// Gives the extension a chance to validate that all options in the extension are valid.
        /// Most extensions do not have invalid combinations and so this will be a no-op.
        /// If options are invalid, then an exception should be thrown.
        /// </summary>
        /// <param name="options">The options being validated.</param>
        public override void Validate(IDbContextOptions options)
        {
            if (AuditUserAccessor == null)
            {
                throw new NoAuditUserAccessorException("Set the accessor when calling UseAuditing, e.g. optionsBuilder.UseAuditing(o => o.UseAuditUserAccesor(new NoAuditUserAuditUserAccessor())); would use an audit accessor designed for when the user isn't being recorded.");
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>A clone of this instance, which can be modified before being returned as immutable.</returns>
        protected override GenericOptionsExtension Clone() => new AuditingOptionsExtension(this);
    }
}
