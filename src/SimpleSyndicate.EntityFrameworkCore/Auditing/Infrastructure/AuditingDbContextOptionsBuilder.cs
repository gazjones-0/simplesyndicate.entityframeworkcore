﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure.Internal;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing.Infrastructure
{
    /// <summary>
    /// <para>
    /// Allows auditing-specific configuration to be performed on the <see cref="DbContextOptions"/>.
    /// </para>
    /// <para>
    /// Instances of this class are returned from a call to
    /// <see cref="AuditingDbContextOptionsBuilderExtensions.UseAuditing(DbContextOptionsBuilder, Action{AuditingDbContextOptionsBuilder})" />
    /// and it is not designed to be directly constructed in your application code.
    /// </para>
    /// </summary>
    public class AuditingDbContextOptionsBuilder : GenericDbContextOptionsBuilder<AuditingDbContextOptionsBuilder, AuditingOptionsExtension>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuditingDbContextOptionsBuilder"/> class.
        /// </summary>
        /// <param name="optionsBuilder">The options builder.</param>
        public AuditingDbContextOptionsBuilder(DbContextOptionsBuilder optionsBuilder)
            : base(optionsBuilder)
        {
        }

        /// <summary>
        /// Configures the context to use the specified <paramref name="auditUserAccessor"/>.
        /// </summary>
        /// <param name="auditUserAccessor">Audit user accessor.</param>
        public virtual void UseAuditUserAccesor(IAuditUserAccessor auditUserAccessor)
        {
            WithOption(e => e.WithAuditUserAccessor(auditUserAccessor));
        }
    }
}
