﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace SimpleSyndicate.EntityFrameworkCore.Auditing
{
    /// <summary>
    /// Helper methods for <see cref="Models.AuditObjectChange"/>.
    /// </summary>
    internal static class AuditObjectChangeHelpers
    {
        /// <summary>
        /// Gets the type name for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>The type name</returns>
        public static string TypeName(EntityEntry entityEntry)
        {
            // if we can't work out the type name, we'll just use whatever's on the type
            var typeName = entityEntry.Entity.GetType().Name;

            // try and get the table name from the custom attribute on the model
            if (entityEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() is TableAttribute tableAttribute)
            {
                typeName = tableAttribute.Name;
            }

            // ensure type name doesn't exceed maximum
            return AuditingHelpers.TrimToLength(typeName, Models.DataAnnotations.AuditObjectChangeDataAnnotations.TypeName.MaxLength);
        }

        /// <summary>
        /// Gets the primary key name for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>The primary key name.</returns>
        public static string PrimaryKeyName(EntityEntry entityEntry)
        {
            var keyNames = entityEntry.Entity.GetType().GetProperties().Where(x => x.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();
            if (keyNames.Count == 0)
            {
                throw new MissingKeyException("No key defined on " + TypeName(entityEntry) + "; did you forget to add [Key] to one of the properties?");
            }

            if (keyNames.Count > 1)
            {
                throw new System.NotSupportedException("Unable to audit " + TypeName(entityEntry) + " as it has a composite key; only single property keys are supported.");
            }

            return keyNames[0].Name;
        }

        /// <summary>
        /// Gets the current value of the primary key for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>The current primary key value.</returns>
        public static string PrimaryKeyCurrentValue(EntityEntry entityEntry)
        {
            var value = entityEntry.CurrentValues[PrimaryKeyName(entityEntry)].ToString();
            return AuditingHelpers.TrimToLength(value, Models.DataAnnotations.AuditObjectChangeDataAnnotations.KeyValue.MaxLength);
        }

        /// <summary>
        /// Gets the original (i.e. before the changes being saved) value of the primary key for the specified <paramref name="entityEntry"/>.
        /// </summary>
        /// <param name="entityEntry">A <see cref="EntityEntry"/> instance.</param>
        /// <returns>The original (i.e. before the changes being saved) primary key value.</returns>
        public static string PrimaryKeyOriginalValue(EntityEntry entityEntry)
        {
            var value = entityEntry.OriginalValues[PrimaryKeyName(entityEntry)].ToString();
            return AuditingHelpers.TrimToLength(value, Models.DataAnnotations.AuditObjectChangeDataAnnotations.KeyValue.MaxLength);
        }
    }
}
