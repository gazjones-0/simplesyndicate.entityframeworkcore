﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// general information
[assembly: AssemblyTitle("SimpleSyndicate.EntityFrameworkCore")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("simpleSyndicate")]
[assembly: AssemblyProduct("SimpleSyndicate.EntityFrameworkCore")]
[assembly: AssemblyCopyright("Copyright © simpleSyndicate 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// version
[assembly: AssemblyVersion("1.0.6")]
[assembly: AssemblyFileVersion("1.0.6")]

// CLS compliance
[assembly: CLSCompliant(false)]

// COM visibility
[assembly: ComVisible(false)]
